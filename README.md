# MOSIP Kernel

## Introduction

MOSIP is Modular Open Source Identity Platform, read and learn more about it in the extensive [documentation](https://github.com/mosip-open/Documentation). The ***seed contribution to MOSIP is still work in progress***, consider this repo as an early preview for now. Today, we aim to give the community a sense of early direction with this preview. 

The issue list is open, but we will not act upon the issues till a formal release is behind us.

MOSIP Kernel is a set of services used across the functional modules. 

## The MOSIP kernel
Start with [Kernel Architecture document](https://github.com/mosip-open/Documentation/wiki/Kernel) and read in detail about the [Kernel API](https://github.com/mosip-open/Documentation/wiki/Kernel-APIs).

