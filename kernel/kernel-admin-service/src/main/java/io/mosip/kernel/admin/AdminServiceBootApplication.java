package io.mosip.kernel.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminServiceBootApplication 
{
    
    /**
     * The main method to start the project.
     *
     * @param args the arguments
     */
    public static void main( String[] args )
    {
        SpringApplication.run(AdminServiceBootApplication.class, args);
    }
}
