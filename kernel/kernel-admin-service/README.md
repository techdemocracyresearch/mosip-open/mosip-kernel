## Admin Service 

[Background & Design](../../docs/design/kernel/kernel-adminservices.md)

[Api Documentation](https://github.com/mosip/mosip/wiki)


Default Port and Context Path

```
server.port=8099
server.servlet.path=/admin

```

localhost:8099/admin/swagger-ui.html


**Application Properties**

[kernel-dev.properties](../../config/kernel-dev.properties)

```

javax.persistence.jdbc.driver=org.postgresql.Driver
javax.persistence.jdbc.url=jdbc:postgresql://localhost:8888/mosip_master
javax.persistence.jdbc.user=dbuser
javax.persistence.jdbc.password=dbpwd


```



