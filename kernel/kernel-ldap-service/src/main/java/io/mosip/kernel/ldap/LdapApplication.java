package io.mosip.kernel.ldap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *  @author Sabbu Uday Kumar
 *  @since 1.0.0
 */
@SpringBootApplication
public class LdapApplication {
	public static void main(String[] args) {
		SpringApplication.run(LdapApplication.class, args);
	}
}
