package io.mosip.kernel.otpnotification.dto;

import lombok.Data;

/**
 * The class for ResponseDto.
 * 
 * @author Ritesh Sinha
 * @since 1.0.0
 */
@Data
public class NotifierResponseDto {
	/**
	 * The status.
	 */
	private String status;
}
