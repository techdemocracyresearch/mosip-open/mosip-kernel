package io.mosip.kernel.otpnotification.dto;

import lombok.Data;

@Data
public class OtpRequestDto {
	private String key;
}
