package io.mosip.kernel.otpnotification.dto;

import lombok.Data;

@Data
public class OtpResponseDto {
	private String otp;
	private String status;
}
