package io.mosip.kernel.otpnotification.dto;

import lombok.Data;

@Data
public class OtpNotificationResponseDto {
	private String status;
	private String message;
}
