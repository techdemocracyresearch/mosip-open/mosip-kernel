/**
 * 
 */
package io.mosip.kernel.auth.entities;

import java.util.List;

import lombok.Data;

/**
 * @author Ramadurai Pandian
 *
 */
@Data
public class UserDetailsRequest {

	List<String> userDetails;

}
