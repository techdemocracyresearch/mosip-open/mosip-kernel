package io.mosip.kernel.auth.entities.otp;

public class OtpValidateRequestDto {
	private String otp;

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
}
